//
//  main.m
//  07_Protocols
//
//  Created by Admin on 31.12.16.
//  Copyright © 2016 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
